Integrating an Amazon Lex Bot with Facebook Messenger
=============================================

### Scenario
This lab use [Amazon Lex](https://aws.amazon.com/tw/lex/) deploy the bot to Facebook Messenger. We will instruct you to sign up [Facebook Developer Account](https://developers.facebook.com/) and create your Facebook Page to fulfill hotel order bot.
Now Facebook require __App Review__, you can launch your bot after done with verification, but we can still practice how to work with Amazon Lex Bot. To see more info [App review](https://developers.facebook.com/docs/app-review).

<p align="center">
    <img  width="500" alt="Integrating-an-Amazon-Lex-Bot-with-Facebook-Messenger.png" src="./images/Integrating-an-Amazon-Lex-Bot-with-Facebook-Messenger.png">
    </p>

## Prerequisites

- Make sure you are in __US East (N. Virginia)__, which short name is __us-east-1__.
  
- Fulfill previous tutorial : [Create a Chatbot and Configure with Intent and Slot type](../Create a Chatbot and Configure with Intent and Slot type)

- Already registered __Facebook Account__


### Create a Facebook Page
- Let's go on [Facebook Page](https://www.facebook.com/pages/create/?ref_type=hc) for create a fan page. 



- Choose __Enterprise business or Brand__, and then click __starte immediately__.

    <p align="center">
        <img  width="600" alt="StartFacebookPage.png" src="./images/StartFacebookPage.png">
    </p>

- For the following dialog box.
    - For __Page Name__, type the __DemoBooking__.
    - For __Category__, type the __Locak Service__.
    - For __Description__, type the __Demo how to create a booking chat bot with Facebook Messenger__.
    - Click __Create Page__.
    - For __Add a Profile Picture__ and __Add a Cover Photo__, just __Skip it__.
    - Click __Save__.

- Click __+ Add a Button__, and choose __Send message__.

    <p align="center">
        <img  width="700" alt="AddButton.png" src="./images/AddButton.png">
    </p>

- Click __Save__.

### Sign up Facebook for Developers
- Let's go on [FaceBook for Developers](https://developers.facebook.com/).

- On the right of the top bar, click __My Apps__. 

- Click __Create App__.

- Choose __Manage Business Integrations__ and click __Continue__.

- Type and choose the following information:
    - App Display name : __DemoBooking__ 
    - App Contact Email : your email
    - App purpose : choose  __Yourself or your own business__
    - Click __Create App__.
    <p align="center">
        <img  width="500" alt="DevelopSetting.png" src="./images/DevelopSetting.png">
    </p>

- In the dashboard, choose __Messenger__ and click __Set Up__.

    <p align="center">
    <img  width="700" alt="SetupMessenger.png" src="./images/SetupMessenger.jpg">
    </p>

- At __Access Tokens__ section, click __Add or Remove Pages__, choose the page you just created, and click __Generate Token__. 

    <p align="center">
    <img  width="700" alt="GenerateToken.png" src="./images/GenerateToken.png">
    </p>

- Check __I understand__ botton, then you can see the token. Keep your __Page Access Token__ because we will use it later.

    <p align="center">
    <img  width="700" alt="Token.png" src="./images/Token.png">
    </p>


- Click __Done__.

- Click __Settings__ in the left navigator, and choose __Basic__.

- Click __Show__ to get __App Secret__.

- Note the __App ID__ and __App Secret__.

    <p align="center">
    <img  width="700" alt="CopyAppID.png" src="./images/CopyAppID.jpg">
    </p>

### Create Lex on AWS

- Sign in __AWS Console__, on the __Service__ menu, click __Lex__.

- In the Bots page, Click  __Create__.

- Select __BookTrip__.

- For Bot name, type the __DemoBookTrip__.

- For Language, choose __English(US)__.

- For COPPA, choose __NO__.

- Click __Create__.

    <p align="center">
    <img  width="700" alt="BotSet.png" src="./images/BotSet.png">
    </p>

### Setting Aliases

- Click __Settings__.

- For Aliases name, type __DemoBooking__.

- For Bot version, choose __Latest__. 

- Click __+__.

    <p align="center">
     <img  width="700" alt="Aliases.png" src="./images/Aliases.png">
    </p>

### Create Channels and connect with Facebook Messenger

- Type the following string :
    - Channel Name : __DemoBooking__
    - Channel Description : __Deploy the bot to FB Messenger__
    - KMS Key : choose __aws/lex__
    - Alias : choose __DemoBooking__
    - Verify Token : __botToken__
    - Page Access Token : paste your __Page Access Token__  
    - App Secret Key :  paste your  __App Secret__  

- Click __Activate__.

- In the __Callback URLs__ part, copy the __Endpoint URL__.

- Back to the FaceBook developer page, on the __Messenger__ tab, click __Settings__.

- On the __Webhooks__ part, click __Add Callback URL__.

- Enter the following informations then click __erify and Save__:
    - Callback URL : the Endpoint URL you copy
    - Verify Token : __botToken__.

    <p align="center">
    <img  width="700" alt="WebhookSetup.png" src="./images/WebhookSetup.png">
    </p>

- Click __Add Subscriptions__.

    <p align="center">
    <img  width="100%" alt="AddSub.jpg" src="./images/AddSub.png">
    </p>

- Choose __Subscription Fields__ (__messages__, __messaging_postbacks__, and __messaging_optins__).

    <p align="center">
    <img  width="500" alt="EditPage.jpg" src="./images/EditPage.png">
    </p>

- Click __Save__.

    

### Test your Chatbot On Messenger 
- Open your Messenger on smartphone and chat with it.

    <p align="center">
    <img  width="50%" alt="massage.png" src="./images/massage.png">
    </p>

## Clean Up

To delete the AWS resources, perform the tasks below in order :
* The bot in Amazon Lex

## Conclusion
Congratulations! We now have learned how to :
- How to create Facebook Page.
- Deploy your Lex bot to Facebook Messenger.

